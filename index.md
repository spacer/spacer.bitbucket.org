SPACER
======

**SPACER** stands for **Software Proof-based Abstraction with CounterExample-based
Refinement**. It is a new algorithmic framework for SMT-based software model checking
using proofs and counterexamples.

Publications
-----

1. *SMT-Based Model Checking for Recursive Programs*, Anvesh Komuravelli, Arie
Gurfinkel and Sagar Chaki, CAV 2014 ([arxiv][cav14_arxiv]|[pdf][cav14_pdf]|[slides][cav14_slides]|[video][cav14_video]).

  * Source Code available from this [fork][spacer_fork] of Z3
  * Download (old) binaries: [Linux x86-64][spacer-2.0-lnx-x86-64] [OS X x86-64][spacer-2.0-osx-x86-64]
  * Benchmarks
        * SLAM Benchmarks from SV-COMP [repository][slam]
        * Python [script][bebop.py] to generate the Boolean Program from [Bebop
          paper][bebop_paper] for a given number of procedures
        * Horn-SMT files for the C programs from [SV-COMP 2014][svcomp14] are available [here][svcomp14-horn]
        * Horn-SMT [example][bd_cmplt] where GPDR diverges for bounded call-stack

[cav14_pdf]: http://www.cs.cmu.edu/~akomurav/publications/spacer2.pdf
[cav14_arxiv]: http://arxiv.org/abs/1405.4028
[cav14_slides]: http://www.cs.cmu.edu/~akomurav/presentations/spacer_cav14.pptx
[cav14_video]: https://mh-engage.ltcc.tuwien.ac.at/engage/ui/watch.html?id=7166658e-124b-11e4-a196-00190f06384e&t=21m03s
[spacer_fork]: https://bitbucket.org/spacer/code
[spacer-2.0-lnx-x86-64]: https://bitbucket.org/spacer/spacer.bitbucket.org/downloads/spacer-2.0-lnx-x86-64.tar.gz
[spacer-2.0-osx-x86-64]: https://bitbucket.org/spacer/spacer.bitbucket.org/downloads/spacer-2.0-osx-x86-64.tar.gz
[slam]: https://svn.sosy-lab.org/software/sv-benchmarks/trunk/clauses/BOOL/slam.zip
[bebop.py]: cav14/bebop.py
[bebop_paper]: http://dx.doi.org/10.1007/10722468_7
[svcomp14]: http://sv-comp.sosy-lab.org/2014/
[svcomp14-horn]: https://bitbucket.org/spacer/svcomp14-horn-smt2
[bd_cmplt]: cav14/bd_cmplt.smt2


2. *Automated Abstraction in SMT-Based Unbounded Software Model
Checking*, Anvesh Komuravelli, Arie Gurfinkel, Sagar Chaki and Edmund M.
Clarke, CAV 2013 ([arxiv][cav13_arxiv]|[pdf][cav13_pdf]|[slides][cav13_slides]).

  * New version of tool: [spacer v1.1][spacer_1_1]. Works with latest unstable version
of [Z3][z3site], as of July 2, 2013.
  * Download the tool: [spacer v0.1][spacer_old]. Tested on Ubuntu 64bit 12.04.1. See
     README file for details.
  * Benchmarks from Software Verification Competition (SV-COMP) are
    available from the competition [repository][svbench].
  * SV-COMP benchmarks in SMT2 format (required to run SPACER) are
    available at [https://bitbucket.org/arieg/svb/downloads/svcomp13-full-smt.zip](https://bitbucket.org/arieg/svb/downloads/svcomp13-full-smt.zip).
  * Experimental Results
      * Z3: [down counter][z3down] and [up counter][z3up].
      * SPACER: [down counter][spacerdown], [up counter][spacerup],
        [PBA-only][spacerpba], and [State-Abstraction][spacersa]

[cav13_pdf]: http://www.cs.cmu.edu/~akomurav/publications/spacer.pdf
[cav13_arxiv]: http://arxiv.org/abs/1306.1945
[cav13_slides]: http://www.cs.cmu.edu/~akomurav/presentations/spacer_cav13.pptx
[spacer_old]: spacer_0.1.tar.gz
[svbench]: https://svn.sosy-lab.org/software/sv-benchmarks/tags/svcomp13
[z3down]: cav13/z3-down-counter.html
[z3up]: cav13/z3-up-counter.html
[spacerdown]: cav13/spacer-down-counter.html
[spacerup]: cav13/spacer-up-counter.html
[spacerpba]: cav13/spacer-pba-only.html
[spacersa]: cav13/spacer-sa.html
[spacer_1_1]: spacer_1.1.tar.gz
[z3site]: http://z3.codeplex.com
