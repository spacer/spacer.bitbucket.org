MARKDOWN = markdown
mdFiles = index.md

all: $(mdFiles:.md=.html)

clean:
	rm -f $(mdFiles:.md=.html)

.SUFFIXES: .md .html
.md.html: header.html footer.html
	(cat header.html; $(MARKDOWN) < $<; cat footer.html) > $@
